<?php
	$logged_in_required = true;
	require_once "header.php";
	$fields = array(
		'level',
		'name',
		'email'
	);

	if($_SESSION['level'] != 0)
	{
		header("Location: index.php?e=1");
	}
	if(isset($_REQUEST['x']))
	{
		$id = $_REQUEST['x'];
	}
	else
	{
		header("Location: manage_users.php?f=1");
	}
	$sql = "SELECT * FROM users WHERE id = '$id'";
	$results = mysql_query($sql, $db);
	while($row = mysql_fetch_assoc($results))
	{	
		$level = $row['level'];
		$name = $row['name'];
		$email = $row['email'];
	}
	$output .= '<tr><td><input type="text" name="level" id="level" value="'.$level.'"></td><td><input type="text" name="name" id="name" value="'.$name.'"></td><td><input type="text" name="email" id="email" value="'.$email.'"></td>';
	if($output != null)
	{
		$output = '<form action="" method="post"><table cellpadding="2" cellspacing="0" border="1"><tr bgcolor="blue"><th>Level</th><th>Name</th><th>Email</th></tr>'.$output.'</table>';
	}
	$level = null;
	if(isset($_REQUEST['level']) && $_REQUEST['level'] != "")
	{
		$level = stripslashes($_REQUEST['level']);
	}
	$newlevel = mysql_real_escape_string($level);

	$name = null;
	if(isset($_REQUEST['name']) && $_REQUEST['name'] != "")
	{
		$name = stripslashes($_REQUEST['name']);
	}
	$newname = mysql_real_escape_string($name);
	
	$email = null;
	if(isset($_REQUEST['email']) && $_REQUEST['email'] != "")
	{
		$email = stripslashes($_REQUEST['email']);
	}
	$newemail = mysql_real_escape_string($email);
	if(isset($_REQUEST['editsubmit']))
	{
		$all_valid = true;
		foreach($fields AS $field)
		{
			if($field == 'level' && !preg_match("/[0-9]/", $_REQUEST[$field]))
			{
				$errors[$field] = "Level must be 0-9";
				$all_valid = false;
			}
			if($field == 'name' && !preg_match("/^[a-zA-Z]+$/", $_REQUEST[$field]))
			{
				$errors[$field] = "The name field must begin with a letter";
				$all_valid = false;
			}
			if($field == 'email' && !preg_match("/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/", $_REQUEST[$field]))
			{
				$errors[$field] = "Not a valid email address";
				$all_valid = false;
			}
		}
		if($all_valid)
		{
			$sql = "UPDATE users SET level = '$newlevel', name = '$newname', email = '$newemail' WHERE id = '$id'";
			$results = mysql_query($sql, $db);
			if(mysql_affected_rows() == 1)
			{
				header("Location: manage_users.php?q=1");
			}
		}
	}
?>
<html>
<head>
<title> Edit User</title>
<script type = "text/javascript" src="blue.js"></script>
<link rel="stylesheet" type="text/css" href="format.css" />

</head>
<body>
 <p class="home">Edit User</p>
 <hr>
<?php
			if($output != null)
			{
				echo $output;
			}
		
		?>
<input class="button" name="editsubmit" type='submit' value='Submit' />
</form>
<?php
			if($errors['level'] != null)
			{
				echo '<span class="error">'.$errors['level'].'</span><br /';
			}
			if($errors['name'] != null)
			{
				echo '<span class="error">'.$errors['name'].'</span><br />';
			}
			if($errors['email'] != null)
			{
				echo '<span class="error">'.$errors['email'].'</span>';
			}
			?>
<?php
			require_once "footer.php";
		?>
</html>
