<?php
	require_once "header.php";
	$fields = array(
		'name',
		'email',
		'message'
	);
	if(isset($_REQUEST['submit']))
	{
		$all_valid = true;
		foreach($fields AS $field)
		{
			if(!isset($_REQUEST[$field]) || empty($_REQUEST[$field]))
			{
				$errors[$field] = "This required field is empty.<br>";
				$all_valid = false;
			}
			else
			{
				if($field == 'name' && !preg_match("/^([A-Za-z0-9\ -]+)$/", $_REQUEST[$field]))
				{
					$errors[$field] = "The name field can consist only of numbers, letters, spaces or hyphens.<br>";
					$all_valid = false;
				}
				if($field == 'email' && (!preg_match("/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/", $_REQUEST[$field])))
				{
					$errors[$field] = "Not a valid email address<br>";
					$all_valid = false;
				}
				if($field == 'message' && (!preg_match("/[\S\s]{10,}/", $_REQUEST[$field])))
				{
					$errors[$field] = "Message must be at least 10 characters long.<br>";
					$all_valid = false;
				}
			}
		}
		if ($all_valid)
  		{
  			$email = $_REQUEST['email'];
  			$name = $_REQUEST['name'];
			$subject = $_REQUEST['subject'];
			$subjectLine = 'AddressBookEnterprises New Message from '.$name.', Subject: '.$subject;
  			$message = $_REQUEST['message'];
  			mail( "test@mailinator.com", "$subjectLine", // This is a PUBLIC email inbox, check at http://test.mailinator.com/
  			$message, "From: $email" );
  			echo "Thank you for using our contact form.";
  		}
	}
?>

<html>
<head>
<title> Contact Us</title>
<link rel="stylesheet" type="text/css" href="format.css" />

</head>
<body>
 <h1 class="home">Contact Us</h1>
 <hr>



<form action="" method="post">
<table>

<td>Name*:</td>
<td><input type="text" name="name" id="name" value="<?php echo $_SESSION['name']; ?>" />
<?php
			if($errors['name'] != null)
			{
				echo '<span class="error">'.$errors['name'].'</span>';
			}
			?>
</td>
<tr>
<td><label for="email">Email*:</label></td>
<td><input type="text" name="email" id="email" value="<?php echo $values['email']; ?>" />
<?php
			if($errors['email'] != null)
			{
				echo '<span class="error">'.$errors['email'].'</span>';
			}
			?>
</td>
<tr>
<td><label for="subject">Subject:</label></td>
<td><input type="text" name="subject" /><br /></td>

<tr>


</tr>
<td>Message*:</td>
<td><textarea rows="5" cols="25" name="message" id="message" value="<?php echo $values['email']; ?>">
</textarea>
<?php
			if($errors['message'] != null)
			{
				echo '<span class="error">'.$errors['message'].'</span>';
			}
			?>
<tr>
<td><td>  <input class="button" name="submit" type='submit' value='Submit' />  </td></td></tr>
</table>
</form>
*This field is required.
</body>
		<?php
			require_once "footer.php";
		?>


</html>
