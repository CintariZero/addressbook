
<?php
	$logged_in_required = true;
	require_once "header.php";
	$fields = array(
		'name',
		'line1',
		'line2',
		'city',
		'state',
		'zip'
	);

	if($_SESSION['level'] != 0)
	{
		header("Location: index.php?e=1");
	}
	if(isset($_REQUEST['x']))
	{
		$id = $_REQUEST['x'];
	}
	else
	{
		header("Location: manage_users.php?f=1");
	}
	$sql = "SELECT * FROM addresses WHERE id = '$id'";
	$results = mysql_query($sql, $db);
	while($row = mysql_fetch_assoc($results))
	{	
		$name = $row['name'];
		$line1 = $row['line1'];
		$line2 = $row['line2'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$status_id = $row['valid'];
	}
	$output .= '<tr><td><input type="text" name="name" id="name" value="'.$name.'"></td><td><input type="text" name="line1" id="line1" value="'.$line1.'"></td><td><input type="text" name="line2" id="line2" value="'.$line2.'"></td><td><input type="text" name="city" id="city" value="'.$city.'"></td><td><input type="text" name="state" id="state" value="'.$state.'"></td><td><input type="text" name="zip" id="zip" value="'.$zip.'"></td>';
	if($output != null)
	{
		$output = '<form action="" method="post"><table cellpadding="2" cellspacing="0" border="1"><tr bgcolor="blue"><th>Name</th><th>Line 1</th><th>Line 2</th><th>City</th><th>State</th><th>Zip</th></tr>'.$output.'</table>';
	}

	$name = null;
	if(isset($_REQUEST['name']) && $_REQUEST['name'] != "")
	{
		$name = stripslashes($_REQUEST['name']);
	}
	$newname = mysql_real_escape_string($name);

	$line1 = null;
	if(isset($_REQUEST['line1']) && $_REQUEST['line1'] != "")
	{
		$line1 = stripslashes($_REQUEST['line1']);
	}
	$newline1 = mysql_real_escape_string($line1);

	$line2 = null;
	if(isset($_REQUEST['line2']) && $_REQUEST['line2'] != "")
		{
			$line2 = stripslashes($_REQUEST['line2']);
		}
	else if(isset($_REQUEST['line2']) && $_REQUEST['line2'] == "")
		{
			$line2 = "&nbsp;";
		}
	$newline2 = mysql_real_escape_string($line2);

	$city = null;
	if(isset($_REQUEST['city']) && $_REQUEST['city'] != "")
	{
		$city = stripslashes($_REQUEST['city']);
	}
	$newcity = mysql_real_escape_string($city);

	$state = null;
	if(isset($_REQUEST['state']) && $_REQUEST['state'] != "")
	{
		$state = stripslashes($_REQUEST['state']);
	}
	$newstate = mysql_real_escape_string($state);

	$zip = null;
	if(isset($_REQUEST['zip']) && $_REQUEST['zip'] != "")
	{
		$zip = stripslashes($_REQUEST['zip']);
	}
	$newzip = mysql_real_escape_string($zip);
	$status = null;
	if(isset($_REQUEST['status']) && $_REQUEST['status'] != "")
	{
		$status = stripslashes($_REQUEST['status']);
	}

	if(isset($_REQUEST['editsubmit']))
	{
		$all_valid = true;
		foreach($fields AS $field)
		{
			if($field == 'name' && !preg_match("/[\S\s]{0,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "The name field must contain letters and numbers, must be below 50 characters";
				$all_valid = false;
			}
			if($field == 'line1' && !preg_match("/[a-zA-Z\s]{1,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Line 1 must contain letters and be fewer than 100 characters";
				$all_valid = false;
			}
			if($field == 'line2' && !preg_match("/[a-zA-Z\s]{0,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Line 2 must contain letters and be fewer than 100 characters";
				$all_valid = false;
			}
			if($field == 'city' && !preg_match("/[a-zA-Z\s]{0,50}/", $_REQUEST[$field]))
			{
				$errors[$field] = "City line must contain letters and be fewer than 50 characters";
				$all_valid = false;
			}
			if($field == 'state' && !preg_match("/[a-zA-Z]{2}/", $_REQUEST[$field]))
			{
				$errors[$field] = "State line must be exactly two characters";
				$all_valid = false;
			}
			if($field == 'zip' && !preg_match("/[0-9]{5,10}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Zip code must be between 5 and 10 characters, must contain numbers";
				$all_valid = false;
			}
		}
		if($all_valid)
		{
			$sql = "UPDATE addresses SET name = '$newname', line1 = '$newline1', line2 = '$newline2', city = '$newcity', state = '$newstate', zip = '$newzip', valid = $status WHERE id = '$id'";
			$results = mysql_query($sql, $db);
			if(mysql_affected_rows() == 1)
			{
				header("Location: manage_listings.php?q=1");
			}
		}
	}
?>
<html>
<head>
<title> Edit User</title>
<script type = "text/javascript" src="blue.js"></script>
<link rel="stylesheet" type="text/css" href="format.css" />

</head>
<body>
 <p class="home">Edit Listing</p>
 <hr>
<?php
			if($output != null)
			{
				echo $output;
			}
		
		?>
<input class="button" name="editsubmit" type='submit' value='Submit' />
<?php
	$status_pulldown = '<select name="status">';
	$sql = "select id, name FROM statuses";
	$results = mysql_query($sql, $db);
	while($row = mysql_fetch_assoc($results))
	{
		$selected = null;
		if($row['id'] == $status_id)
		{
			$selected = " selected ";
		}
		
		$status_pulldown .= '<option value="'.$row['id'].'" '.$selected.'>'.$row['name'].'</option>';
	}
	$status_pulldown .= "</select>";
	echo $status_pulldown;
	?>
</form>
<?php
			if($errors['name'] != null)
			{
				echo '<span class="error">'.$errors['name'].'</span><br />';
			}
			if($errors['line1'] != null)
			{
				echo '<span class="error">'.$errors['line1'].'</span><br />';
			}
			if($errors['line2'] != null)
			{
				echo '<span class="error">'.$errors['line2'].'</span>';
			}
			if($errors['city'] != null)
			{
				echo '<span class="error">'.$errors['city'].'</span>';
			}
			if($errors['state'] != null)
			{
				echo '<span class="error">'.$errors['state'].'</span>';
			}
			if($errors['zip'] != null)
			{
				echo '<span class="error">'.$errors['zip'].'</span>';
			}
			?>
			
			
			

<?php
			require_once "footer.php";
		?>
</html>
