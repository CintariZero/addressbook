-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 01, 2010 at 08:30 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `addressbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `valid` int(1) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `line1` varchar(100) NOT NULL,
  `line2` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(11) NOT NULL,
  `zip` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `created`, `valid`, `user_id`, `name`, `line1`, `line2`, `city`, `state`, `zip`) VALUES
(1, '2010-11-01 13:53:25', 1, 10, 'Home', '123 Fake Street', '&nbsp;', 'Myrtle Beach', 'SC', '29579'),
(3, '2010-11-01 18:56:55', 1, 10, 'Apartment', '4845 Meadowsweet Dr', 'Unit 1408', 'Myrtle Beach', 'SC', '29579'),
(4, '2010-11-01 19:20:18', 1, 10, 'Real Home', '430 Emerson Dr', '&nbsp;', 'Myrtle Beach', 'SC', '29579'),
(6, '2010-11-08 20:42:05', 1, 10, 'Next', '1500 Pennsylvania Avenue', '&nbsp;', 'Washington', 'DC', '20500'),
(8, '2010-11-09 00:21:58', 1, 12, 'My place', '111 One lane', '&nbsp;', 'Twoson', 'EL', '23009'),
(19, '2010-12-01 11:01:18', 1, 10, 'Waffle', '101 Syrup', ' ', 'Blue Lake', 'MO', '45454'),
(14, '2010-11-15 15:38:07', 0, 8, 'Pancake', '101 Address', ' ', 'San Francisco', 'CA', '94130'),
(20, '2010-12-01 11:10:57', 2, 10, 'Pending!?', '222 Pending Avenue', ' ', 'Pentagon', 'PA', '99999'),
(18, '2010-11-15 16:17:41', 1, 12, 'Newaddress', '515 Address', '&nbsp;', 'New Zealand', 'PO', '00000');
