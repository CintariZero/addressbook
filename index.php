<?php
	require_once "header.php";
	if(isset($_REQUEST['e']))
	{
		$msg = "You do not have permission to access that page<br />";
		echo $msg;
	}
?>


<html>
<head class="padding">
<title>Address Book</title>
<link rel="stylesheet" type="text/css" href="format.css" />
</head>
<body>
<a class="home">Address Book Enterprises</a>
  <hr>
  <p class="tiny">A Division of RobCo Enterprises<p><center>
<h1 class="padding">Welcome to Address Book Enterprises</a></h1></center>
	<?php
	if(isset($_SESSION['name']))
	{
		echo '<p class="padding"> Welcome to AddressBookEnterprises! This is a public address book which accepts address contributions from anyone. Click "address listings" below to see the listings so far, or create an account to add new listings. Enjoy!</p>';
	}
	else
	{
		echo 'Welcome back ', $_SESSION['name'], '! Access your address listing by clicking the "address listings" link below.';
	}

	?>

<!--
<footer><a href="privacy.html">Privacy Policy</a> | <a href="contact.php">Contact Us</a><br> &copy; 2010 RobCo Enterprises</footer>
-->
</body>
		<?php
			require_once "footer.php";
		?>
</html>
