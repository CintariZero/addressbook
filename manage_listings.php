<?php
	$logged_in_required = true;
	require_once "header.php";
	if(isset($_REQUEST['r']))
	{
		echo "Address inserted successfully";
		echo $msg;
	}
	if(isset($_REQUEST['q']))
	{
		$msg = "Successfully updated address data!";
		echo $msg;
	}
	$fields = array(
		'name',
		'line1',
		'line2',
		'city',
		'state',
		'zip'
	);
	$id = $_SESSION['id'];
	if ($_SESSION['level'] == 1)
	{
		$sql = "SELECT * FROM addresses WHERE user_id = '$id'";
	}
	else if ($_SESSION['level'] == 0)
	{
		$sql = "SELECT * FROM addresses";
	}
	$results = mysql_query($sql, $db);

	while($row = mysql_fetch_assoc($results))
	{	
		if($row['valid'] == 0)
		{
			$toggled = "<i>inactive</i>";
		}
		else if($row['valid'] == 1)
		{
			$toggled = "<b>Active</b>";
		}
		else if($row['valid'] == 2)
		{
			$toggled = "<i>Pending</i>";
		}

		$name = $row['name'];
		$line1 = $row['line1'];
		$line2 = $row['line2'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$edit = '<center><a class="pumpkin" href="edit_listing.php?x='.$row['id'].'">[x]</a></center>';
		$delete = '<center><a class="pumpkin" href="delete_listing.php?x='.$row['id'].'">[x]</a></center>';
		$toggle = '<center><a class="pumpkin" href="toggle.php?x='.$row['id'].'">'.$toggled.'</a></center>';
		$output .= "<tr><td>$toggle</td><td>$name</td><td>$line1</td><td>$line2</td><td>$city</td><td>$state</td><td>$zip</td><td>$edit</td><td>$delete</td></tr>\n";
	}
	if($output != null)
	{
		$output = '<table cellpadding="2" cellspacing="0" border="1"><tr bgcolor="blue"><th>Status</th><th>Name</th><th>Line 1</th><th>Line 2</th><th>City</th><th>State</th><th>Zip</th><th>Edit</th><th>Delete</th></tr>'.$output.'</table>';
	}
	if(isset($_REQUEST['submit']))
	{
	$all_valid = true;
		foreach($fields AS $field)
		{

			if($field == 'name' && !preg_match("/[\S\s]{0,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "The name field must contain letters and numbers, must be below 50 characters";
				$all_valid = false;
			}
			if($field == 'line1' && !preg_match("/[a-zA-Z\s]{1,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Line 1 must contain letters and be fewer than 100 characters";
				$all_valid = false;
			}
			if($field == 'line2' && !preg_match("/[a-zA-Z\s]{0,100}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Line 2 must contain letters and be fewer than 100 characters";
				$all_valid = false;
			}
			if($field == 'city' && !preg_match("/[a-zA-Z\s]{0,50}/", $_REQUEST[$field]))
			{
				$errors[$field] = "City line must contain letters and be fewer than 50 characters";
				$all_valid = false;
			}
			if($field == 'state' && !preg_match("/[a-zA-Z]{2}/", $_REQUEST[$field]))
			{
				$errors[$field] = "State line must be exactly two characters";
				$all_valid = false;
			}
			if($field == 'zip' && !preg_match("/[0-9]{5,10}/", $_REQUEST[$field]))
			{
				$errors[$field] = "Zip code must be between 5 and 10 characters, must contain numbers";
				$all_valid = false;
			}
			
		}

		$name = null;
		if(isset($_REQUEST['name']) && $_REQUEST['name'] != "")
		{
			$name = stripslashes($_REQUEST['name']);
		}
		$safe_name = mysql_real_escape_string($name);
		
		$line1 = null;
		if(isset($_REQUEST['line1']) && $_REQUEST['line1'] != "")
		{
			$line1 = stripslashes($_REQUEST['line1']);
		}
		$safe_line1 = mysql_real_escape_string($line1);
		
		$line2 = null;
		if(isset($_REQUEST['line2']) && $_REQUEST['line2'] != "")
		{
			$line2 = stripslashes($_REQUEST['line2']);
		}
		else if(isset($_REQUEST['line2']) && $_REQUEST['line2'] == "")
		{
			$line2 = "&nbsp;";
		}
		$safe_line2 = mysql_real_escape_string($line2);

		$city = null;
		if(isset($_REQUEST['city']) && $_REQUEST['city'] != "")
		{
			$city = stripslashes($_REQUEST['city']);
		}
		$safe_city = mysql_real_escape_string($city);

		$state = null;
		if(isset($_REQUEST['state']) && $_REQUEST['state'] != "")
		{
			$state = stripslashes($_REQUEST['state']);
		}
		$safe_state = mysql_real_escape_string($state);

		$zip = null;
		if(isset($_REQUEST['zip']) && $_REQUEST['zip'] != "")
		{
			$zip = stripslashes($_REQUEST['zip']);
		}
		$safe_zip = mysql_real_escape_string($zip);
		$status = null;
		if(isset($_REQUEST['status']) && $_REQUEST['status'] != "")
		{
			$status = stripslashes($_REQUEST['status']);
		}
		$safe_id = stripslashes($id);
		if ($all_valid){
			$sql = "INSERT INTO addresses SET name = '$safe_name', line1 = '$safe_line1', line2 = '$safe_line2', city = '$safe_city',state = '$safe_state', zip = '$safe_zip', user_id = '$safe_id', valid = '$status', created=NOW()";
			$results = mysql_query($sql, $db);
			$id = 0;
			$id = mysql_insert_id($db);
			if($id != 0)
			{
				header("Location: manage_listings.php?r=1");
			}
		}
	}
?>
<html>
	<head>
		<title>Address Book</title>
		<link rel="stylesheet" type="text/css" href="format.css" />
	</head>
	<body>
		
		<h1><?php echo $_SESSION['name']; ?>'s Address Book Listings</h1><br />
<div class="tiny">Powered by Address Book Enterprises</div>
<?php
			if($output != null)
			{
				echo $output;
			}
		
		?>
Click "Active" or "inactive" to toggle between active and inactive.
<br />
<br />
<table>
Register a new address:
<form action="" method="post">
<td><label for="name">Name:</td>
<td><input type="text" name="name" id="name" value="">
</label>
<?php
			if($errors['name'] != null)
			{
				echo '<span class="error">'.$errors['name'].'</span>';
			}
			?>
</td>
<tr>
<td><label for="line1">Line 1:</td>
<td><input type="text" name="line1" id="line1" value="" />
</label>
<?php
			if($errors['line1'] != null)
			{
				echo '<span class="error">'.$errors['line1'].'</span>';
			}
			?>
<tr>
<td><label for="line2">Line 2:</td>
<td><input type="text" name="line2" id="line2" value="" />
</label>
<?php
			if($errors['line2'] != null)
			{
				echo '<span class="error">'.$errors['line2'].'</span>';
			}
			?>
</tr>
<tr>
<td><label for="city">City:</td>
<td><input type="text" name="city" id="city" value="" />

</label>
<?php
			if($errors['city'] != null)
			{
				echo '<span class="error">'.$errors['city'].'</span>';
			}
			?>
</tr>
<tr>
<td><label for="state">State:</td>
<td><input type="text" name="state" id="state" value="" />
</label>
<?php
			if($errors['state'] != null)
			{
				echo '<span class="error">'.$errors['state'].'</span>';
			}
			?>
</tr>
<tr>
<td><label for="zip">Zip:</td>
<td><input type="text" name="zip" id="zip" value="" />
</label>
<?php
			if($errors['zip'] != null)
			{
				echo '<span class="error">'.$errors['zip'].'</span>';
			}
			?>
</tr>
<tr>
<td><input class="button" name="submit" type='submit' value='Submit' /></td><td>
<?php
	$status_pulldown = '<select name="status">';
	$sql = "select id, name FROM statuses";
	$results = mysql_query($sql, $db);
	while($row = mysql_fetch_assoc($results))
	{
		$selected = " selected ";
		$status_pulldown .= '<option value="'.$row['id'].'" '.$selected.'>'.$row['name'].'</option>';
	}

	$status_pulldown .= "</select>";
	echo $status_pulldown;
	?>
</td>
</tr>
</table>
</form>
		
</body>
<?php
		require_once "footer.php";
	?>
</html>
