<?php
	require_once "header.php";
	if(isset($_REQUEST['e']))
	{
		$msg = "You must register or login before accessing that page";
		echo $msg;
	}


	$fields = array(
		'name',
		'email',
		'password',
		'retype'
	);
	if(isset($_REQUEST['submit']))
	{
		$all_valid = true;
		foreach($fields AS $field)
		{
			if(!isset($_REQUEST[$field]) || empty($_REQUEST[$field]))
			{
				$errors[$field] = "This field is required.";
				$all_valid = false;
			}
			else
			{
				if($field == 'name' && !preg_match("/^[a-zA-Z]+$/", $_REQUEST[$field]))
				{
					$errors[$field] = "The name field must begin with a letter";
					$all_valid = false;
				}
				if($field == 'email' && !preg_match("/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/", $_REQUEST[$field]))
				{
					$errors[$field] = "Not a valid email address";
					$all_valid = false;
				}
				if($field == 'password' && !preg_match("/[\S\s]{6,}/", $_REQUEST[$field]))
				{
					$errors[$field] = "Password must be at least 6 characters";
					$all_valid = false;
				}
				if($field == 'retype' && Strcmp($_REQUEST[$field], $_REQUEST['password']))
				{
					$errors[$field] = "Must match password field exactly";
					$all_valid = false;
				}
			}
		}
		$name = null;
		if(isset($_REQUEST['name']) && $_REQUEST['name'] != "")
		{
			$name = stripslashes($_REQUEST['name']);
		}
		$safe_name = mysql_real_escape_string($name);
		
		$email = null;
		if(isset($_REQUEST['email']) && $_REQUEST['email'] != "")
		{
			$email = stripslashes($_REQUEST['email']);
		}
		$safe_email = mysql_real_escape_string($email);

		$password = null;
		if(isset($_REQUEST['password']) && $_REQUEST['password'] != "")
		{
			$password = stripslashes($_REQUEST['password']);
		}
		$safe_password = mysql_real_escape_string($password);

		if($all_valid)
		{
			$sql = "SELECT name FROM users WHERE name = '$safe_name'";
			$results = mysql_query($sql, $db);
			$found = mysql_num_rows($results);
		
			if($found == 0)
			{
				$sql = "INSERT INTO users SET name = '$safe_name', email = '$safe_email', password = '$safe_password', created=NOW(), level = 1";
				$results = mysql_query($sql, $db);
				$id = 0;
				$id = mysql_insert_id($db);
				if($id != 0)
				{
					echo "Success!";
				}
			}
			else
			{
				echo "Error: Name already exists";
				$name = "";
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN">

<html>
<head>
<title> User Registration</title>
<script type = "text/javascript" src="blue.js"></script>
<link rel="stylesheet" type="text/css" href="format.css" />

</head>
<body>
 <p class="home">User Registration</p>
 <hr>

<form method="post" action="" onsubmit="return validation()">
<div id="help"></div>
<table>
<div id="req"></div>
<td><label for="name">Name:</td>
<td><input type="text" name="name" id="name" value="" onfocus="display('Enter a name.');" onblur="clear_help();requires('name');"  />
<span class="errormsg" id="e_name"></span>
<?php

			if($errors['name'] != null)
			{
				echo '<span class="error">'.$errors['name'].'</span>';
			}
			?>
</label>
</td>
<tr>
<td><label for="email">Email:</td>
<td><input type="text" name="email" id="email" value="" onfocus="display('Enter an email address');" onblur="clear_help();requires('email');" />
<span class="errormsg" id="e_email"></span>
<?php
			if($errors['email'] != null)
			{
				echo '<span class="error">'.$errors['email'].'</span>';
			}
			?>
</label>
</td>
<tr>
<td><label for="password">Password:</td>
<td><input type="password" name="password" id="password" value="" onfocus="display('Enter a password');" onblur="clear_help();requires('password');" />
<span class="errormsg" id="e_password"></span>
<?php
			if($errors['password'] != null)
			{
				echo '<span class="error">'.$errors['password'].'</span>';
			}
			?>
</label>
</td>
<tr>
<td><label for="retype">Retype password:</label></td>
<td><input type="password" name="retype" id="retype" value="" onfocus="display('Retype your password');" onblur="clear_help();requires('retype');" />
<span class="errormsg" id="e_retype"></span>
<?php
			if($errors['retype'] != null)
			{
				echo '<span class="error">'.$errors['retype'].'</span>';
			}
			?>
</tr>
<br />
<tr>
<td><input class="button" name="submit" type='submit' value='Submit' /></td> 
</tr>
</table>
</form>

<!--
<footer><a href="privacy.html">Privacy Policy</a> | <a href="contact.html">Contact Us</a><br>&copy; 2010 RobCo Enterprises</footer>
-->
</body>
		<?php
			require_once "footer.php";
		?>
</html>
